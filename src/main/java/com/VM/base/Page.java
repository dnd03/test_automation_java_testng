package com.VM.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class Page {

	public static WebDriver driver;

	//public static TopMenu topMenu;

	public static FooterMenu footerMenu;

	public static Properties Config = new Properties();

	public static Properties OR = new Properties();

	public static FileInputStream fis;

	public static Logger log = Logger.getLogger("devpinoyLogger");

	public static WebDriverWait wait;

	public static String browser;

	public static ExtentTest test;

	public static ExtentReports rep;

	
	@BeforeSuite
	public void setUp() {
		if (driver == null) {
			
			
			try {

				fis = new FileInputStream(System.getProperty("user.dir")
						+ "\\src\\test\\resources\\com\\VM\\properties\\Config.properties");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			try {
				Config.load(fis);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				fis = new FileInputStream(
						System.getProperty("user.dir") + "\\src\\test\\resources\\com\\VM\\properties\\OR.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				OR.load(fis);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Jenkins Browser Filter confiduration
			if(System.getenv("browser")!=null && !System.getenv("browser").isEmpty())
			{
				browser = System.getenv(browser);
				
			}
			else
			{
				browser = Config.getProperty("browser");
			}
			
			Config.setProperty("browser", browser);
			
			if(Config.getProperty("browser").equals("firefox"))
			{
			
				System.setProperty("wedriver.gecko.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\com\\VM\\executables\\geckodriver.exe");
				driver = new FirefoxDriver();	
			}
			else if(Config.getProperty("browser").equals("chrome"))
			{
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\test\\resources\\com\\VM\\executables\\chromedriver.exe");

			driver = new ChromeDriver();
			}
			else if(Config.getProperty("browser").equals("ie"))
			{
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\com\\VM\\executables\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
		
			driver.get(Config.getProperty("testsiteurl"));

			driver.manage().window().maximize();

			driver.manage().timeouts().implicitlyWait(Integer.parseInt(Config.getProperty("implicit.wait")), TimeUnit.SECONDS);

			wait = new WebDriverWait(driver, 5);
			
			//topMenu = new TopMenu();

			footerMenu = new FooterMenu();
			footerMenu.toVerifyFooterSectionLinksStatus();
		
		
		}
	}
	
	
	// To check weather element is present or not
	public boolean isElementPresent(By by)
	{
		try
		{
			driver.findElement(by);
			return true;
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}
	
	//To check weather element is enabeld or disabled
	public boolean isElementEnabledOrDisabled(By by)
	{
		try
		{
			driver.findElement(by).isEnabled();
			System.out.println("Button Enabled!!");
			return true;
		}
		catch(NoSuchElementException e)
		{
			
			System.out.println("Button Disabled!!!");
			return false;
			// it means button disabled.
		}
		
	}
	
	
	
	public static void quit()
	{
		
		driver.quit();
	}
	
	

}
