package com.VM.base;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class FooterMenu extends Page {

	public void toVerifyFooterSectionLinksStatus() {
		WebElement footerBlock = driver.findElement(By.tagName("footer"));
		List<WebElement> links = footerBlock.findElements(By.tagName("a"));
		System.out.println("Total Links are " + links.size());
		int invalidlinkscount = 0;
		for (WebElement link : links) {

			if (link != null) {
				String url = link.getAttribute("href");
				if (!url.contains("mailto:service@veromoda.com.my")) {
					verifyLinkActive(url);

				} else if (url.contains("mailto:service@veromoda.com.my")) {
					continue;
				} else {
					invalidlinkscount++;
				}

			} else {
				System.out.println("Null Links are " + link);
			}

		}

	}

	public void goTOFaqSection() {

	}

	public void goToShippingSection() {

	}

	public void goToReturnSection() {

	}

	public void goToOurStorySection() {

	}

	public static void verifyLinkActive(String link) {

		try {
			URL url = new URL(link);

			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

			httpURLConnection.setConnectTimeout(3000);
			httpURLConnection.connect();

			if (httpURLConnection.getResponseCode() == 200) {
				System.out.println(link + " - " + httpURLConnection.getResponseMessage());

			} else if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
				System.out.println(link + " - " + httpURLConnection.getResponseMessage() + " - "
						+ HttpURLConnection.HTTP_NOT_FOUND);

			} else if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_FORBIDDEN) {
				System.out.println(link + " - " + httpURLConnection.getResponseMessage() + " - "
						+ HttpURLConnection.HTTP_FORBIDDEN);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
