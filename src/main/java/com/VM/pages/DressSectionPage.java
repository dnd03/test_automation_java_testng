package com.VM.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.VM.base.Page;

public class DressSectionPage extends Page {
	
	
	
	public void goToProductViewMoreOnParticularProduct()
	{
		WebElement element = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		driver.findElement(By.linkText(OR.getProperty("viewMoreLink"))).click();
		
		
	}
	
	public void toVerifyAddToBagDisbaledWithoutSelectSize()
	{
		WebElement element = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		isElementEnabledOrDisabled(By.linkText("ADD TO BAG"));
		/*WebElement demodiv = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		System.out.println(demodiv.getAttribute("innerHTML"));
		
		System.out.println(demodiv.getAttribute("textcontent"));
	*/
	}
	
	public void toVerifyAddToBagEnabledWithSelectSize() throws InterruptedException
	{
		WebElement element = driver.findElement(By.cssSelector(OR.getProperty("productElement")));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		/*//List<WebElement> options = driver.findElements(By.cssSelector("select.custom-select.form-control.select-size"));
		System.out.println(options.size());
 		for(WebElement sizeOption : options)
 		{
 			if(sizeOption.getText().equals("XS"));
 			System.out.println(sizeOption.getText());
 			sizeOption.click();
 			
 		}
 		isElementEnabledOrDisabled(By.linkText("ADD TO BAGS"));
		*/
		
		Select se = new Select(driver.findElement(By.cssSelector("select.custom-select.form-control.select-size")));
		se.selectByVisibleText("M");
		Thread.sleep(4000);
		driver.findElement(By.xpath("/html/body/div[1]/div[3]/div/div[2]/div/div/div[3]/div[2]/div/div[1]/div/div/div[1]/div/div[2]/div[1]/div/div[2]/div/div/button")).click();
		
		
	}
	

}
