package com.VM.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.VM.base.FooterMenu;
import com.VM.base.Page;
import com.VM.pages.DressSectionPage;
import com.VM.pages.HomePage;

public class ProductAddCartTest extends BaseTest {

	@Test(priority = 1)
	public void addCart() {
		SoftAssert softAssertion = new SoftAssert();
		HomePage home = new HomePage();
		// home.goToDressBuysection();
		home.goToDressBuysection();
		softAssertion.assertEquals("Dress | Vero Moda Malaysia", driver.getTitle());
		softAssertion.assertAll();
		// Assert.assertTrue(condition);

		footerMenu = new FooterMenu();
		footerMenu.toVerifyFooterSectionLinksStatus();
	}

	@Test(priority = 2)
	public void verifyAddToBagDisbaledWithoutSelectSize() {

		DressSectionPage dressSection = new DressSectionPage();
		dressSection.toVerifyAddToBagDisbaledWithoutSelectSize();

	}

	@Test(priority = 3)
	public void demo() throws InterruptedException {

		DressSectionPage dressSection = new DressSectionPage();

		// dressSection.toVerifyAddToBagDisbaledWithoutSelectSize();

		dressSection.toVerifyAddToBagEnabledWithSelectSize();
/*
		DressSectionPage dressSection = new DressSectionPage();
		dressSection.goToProductViewMoreOnParticularProduct();
	*/
	}

}
