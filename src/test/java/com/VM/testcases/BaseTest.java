package com.VM.testcases;

import java.sql.Driver;

import org.testng.annotations.AfterSuite;

import com.VM.base.Page;

public class BaseTest extends Page {
	
	@AfterSuite
	public void tearDown()
	{
		Page.quit();
		
	}

}
